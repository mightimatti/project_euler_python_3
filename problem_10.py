"""
Problem 10

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
"""

import functools
import math

def construct_primes_list_to(upper_bound):
	"""
	 construct list of primes using sieve of Erastothenes
	"""
	# initiate structure as dict, to allow faster access to elements
	# memory constrained
	ret_dict ={ i : True  for i in range(2,upper_bound+1)}
	loop_limit = int(math.sqrt(upper_bound))
	# eliminate entries that are not prime
	for n in range(2, loop_limit+1):
		for m in range(2*n, upper_bound+1, n):
			ret_dict[m] = False
	ret_list = [] 
	# convert dict into list
	for i,j in ret_dict.items():
		if j:
			ret_list.append(i)
	return sorted(ret_list)


if __name__ == '__main__':
	upper_bound = 2 * (10 **6)
	primes = construct_primes_list_to(upper_bound)
	summ = functools.reduce(
		lambda x,y: x+y,
		primes, 
		0
	)
	print(summ)