"""
Problem 6

The sum of the squares of the first ten natural numbers is,
12+22+...+102=385

The square of the sum of the first ten natural numbers is,
(1+2+...+10)2=552=3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025−385=2640

.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum."""

def add(x,y):
	return x+y

def sum_squares(x):
	summ = 0
	for n in range(x):
		n += 1
		summ += n **2
	return summ


def square_sum(x):
	summ = 0
	for n in range(x):
		n += 1
		summ += n 
	summ = summ**2

	return summ

if __name__ == '__main__':
	upper_bound = 100
	print(square_sum(100) - sum_squares(100))