"""
Problem 14

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?
"""

sequences = {
		4 : 3,
		2 : 2
	}

used_abbr = 0
called_rec = 0

def generate_next_sequence_element(n):
	if n % 2:
		return 3 * n + 1
	else: 
		return n / 2

def determine_length_of_chain(n):
	if n in sequences.keys():
		return sequences[n]
	else:
		ne = generate_next_sequence_element(n)
		try:
			if n % 2 == 0:
				loc_ne = sequences[n/2] + 1
				used_abbr += 1
			else:
				loc_ne = sequences[(3 * n + 1)//2] + 2
				used_abbr += 1
		except KeyError:
			loc_ne = determine_length_of_chain(ne) + 1
			called_rec +=1

		sequences[n] =  loc_ne
		return loc_ne



def main():
	num = -1
	maxim = 0
	for i in range(2,10**6):
		i = 10**6 - i
		loc = determine_length_of_chain(i)
		if loc > maxim:
			maxim = loc
			num = i
		# print("{}:\t{}".format(i, loc))
	
	print(num)
	print("used_abbr", used_abbr)
	print("called_rec", called_rec)

if __name__ == "__main__":
	main()