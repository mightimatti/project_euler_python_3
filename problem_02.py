def fib_sum(upper_bound):
	i = 0
	j = 1
	sum = 0
	while i < upper_bound:
		if  j % 2 == 0:
			sum += j
		tmp = i + j
		i = j
		j = tmp
	return sum

if __name__ == '__main__':
	print(fib_sum(4*10**6))