"""
Problem 14

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?
"""

sequences = {
		4 : {
			"next" : 2,
			"remaining" : 3
		},
		2 : {
			"next" : 1,
			"remaining" : 2
		} 
	}

def generate_next_sequence_element(n):
	if n % 2:
		return 3 * n + 1
	else: 
		return n / 2

def determine_length_of_chain(n):
	if n in sequences.keys():
		return sequences[n]['remaining']
	else:
		ne = generate_next_sequence_element(n)
		loc_ne = determine_length_of_chain(ne) + 1
		sequences[n] = {
			"next" : ne,
			"remaining" : loc_ne
		}
		return loc_ne



def main():
	num = -1
	maxim = 0
	for i in range(2,10**6):
		loc = determine_length_of_chain(i)
		if loc > maxim:
			maxim = loc
			num = i
		# print("{}:\t{}".format(i, loc))
	print(num)
	print(len(sequences.keys()))
if __name__ == "__main__":
	main()