"""
10001st prime
   
Problem 7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""
import math

def construct_primes_list_to(upper_bound):
	"""
	 construct list of primes using sieve of Erastothenes
	"""
	# initiate structure as dict, to allow faster access to elements
	# memory constrained
	ret_dict ={ i : True  for i in range(2,upper_bound+1)}
	loop_limit = int(math.sqrt(upper_bound))
	# eliminate entries that are not prime
	for n in range(2, loop_limit+1):
		for m in range(2*n, upper_bound+1, n):
			ret_dict[m] = False
	ret_list = [] 
	# convert dict into list
	for i,j in ret_dict.items():
		if j:
			ret_list.append(i)
	return sorted(ret_list)

if __name__ == '__main__':
	primes = construct_primes_list_to(10**6)
	print(len(primes))
	print(primes[5])
	print(primes[10000])