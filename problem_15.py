from math import factorial

if __name__ == "__main__":
	for i in range(1,21):
		n = 2*i
		k = i
		print(factorial(n) // (factorial(k) * factorial(n-k)))