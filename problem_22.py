import re

def determine_alphabetical_value(word):
	return sum(map(lambda x:  ord(x) - 64, word))


def open_file(path_to_file):
	list_of_names = []
	with open(path_to_file) as f:
		for l in f.read().split(","):
			assert re.match("^\"[A-Z]+\"$", l)
			l = l[1:-1]
			list_of_names.append(
				(l, determine_alphabetical_value(l),)
			)
	return list_of_names

def sort_and_map(l):
	sorted_list = sorted(l, key=lambda tup: tup[0])
	summ = 0
	for ind, tup in enumerate(sorted_list):
		summ += (ind+1) * tup[1]
	return summ

def main():
	tups = open_file("assets/p022_names.txt")
	# for t in tups:
	# 	if len(t[0]) == 3:
	# 		print(t)
	print(sort_and_map(tups))

if __name__ == '__main__':
	main()
