"""
Problem 5
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

import math

def construct_primes_list_to(upper_bound):
    """
     construct list of primes using sieve of Erastothenes
    """
    # initiate structure as dict, to allow faster access to elements
    # memory constrained
    ret_dict ={ i : True  for i in range(2,upper_bound+1)}
    loop_limit = int(math.sqrt(upper_bound))
    # eliminate entries that are not prime
    for n in range(2, loop_limit+1):
        for m in range(2*n, upper_bound+1, n):
            ret_dict[m] = False
    ret_list = [] 
    # convert dict into list
    for i,j in ret_dict.items():
        if j:
            ret_list.append(i)
    return sorted(ret_list)

if __name__ == '__main__':
    
    upper_bound = 20
    res = 1
    # multiply all primefactos
    for x in construct_primes_list_to(10): 
        res *= x
    for x in range(upper_bound):
        cur = upper_bound - x
        if res % cur == 0:
            continue
        else:
            for x in range(2, upper_bound):
                if (x * res) % cur == 0:
                    res *= x
                    break
    print(res)

    # prod = 1
    # upper_bound = 10
    # for x in range(upper_bound):
    #     cur = upper_bound - x
    #     if prod % cur == 0:
    #         continue
    #     else:
    #         prod *= cur
    # print(prod)

