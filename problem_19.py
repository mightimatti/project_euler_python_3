"""
Problem 19

You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
"""
import arrow

def main():
	start = arrow.get(1901, 1, 1)
	end = arrow.get(2000, 12, 31)

	cur = start 
	sunday_count = 0
	while cur < end:
		if cur.format('d') == '7':
			sunday_count += 1
		cur = cur.shift(months=+1)
	return sunday_count	

if __name__ == '__main__':
	print(main())