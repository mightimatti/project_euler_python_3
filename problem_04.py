
"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.

"""

def find_palindromic_product(digits):
	pals = []
	attempts = 10**digits - 10**(digits-1)
	start = int("9"*digits)
	for i in range(attempts):
		for j in range(attempts):
			print("checking: {}, {}".format((start-i),(start-j) ))
			# calculate product
			tmp = (start-i)*(start-j) 
			# check if palindrome
			as_list = list(str(tmp))
			rev = as_list.copy()
			rev.reverse()
			if rev == as_list:
				pals. append(((start-i), (start-j), tmp,))
	return pals

if __name__ == '__main__':
	products = set()
	pals = find_palindromic_product(3)
	for pal in pals:
		products.add(pal[2])
		print("{0} x {1} = {2}".format(*pal))
	print(sorted(list(products))[-1])


"""				return  {
					"x" : (start-i),
				 	"y" : (start-j),
				 	"z" : tmp
			 	}

if __name__ == '__main__':
	print("{x} x {y} = {z}".format(find_palindromic_product(3)))
"""