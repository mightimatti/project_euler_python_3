"""
	Problem 3
	The prime factors of 13195 are 5, 7, 13 and 29.
	What is the largest prime factor of the number 600851475143 ?
"""
import math

def construct_primes_list_to(upper_bound):
	"""
	 construct list of primes using sieve of Erastothenes
	"""
	# initiate structure as dict, to allow faster access to elements
	# memory constrained
	ret_dict ={ i : True  for i in range(2,upper_bound+1)}
	loop_limit = int(math.sqrt(upper_bound))
	# eliminate entries that are not prime
	for n in range(2, loop_limit+1):
		for m in range(2*n, upper_bound+1, n):
			ret_dict[m] = False
	ret_list = [] 
	# convert dict into list
	for i,j in ret_dict.items():
		if j:
			ret_list.append(i)
	return sorted(ret_list)



if __name__ == '__main__':
	# to_factorize = 600851475143
	to_factorize = 13195
	primes = construct_primes_list_to(int(to_factorize/2))
	print("Checking {} primes".format(len(primes)))
	# iterate primes in reverse order until prime factor is found
	print(primes)
	for cand in reversed(primes):
		if to_factorize % cand == 0:
			print(cand)
			print("Index: {}".format(primes.index(cand)))
			exit()

# if __name__ == '__main__':
# 	to_factorize = 600851475143
# 	primes = construct_primes_list_to(int(math.sqrt(to_factorize))+1)
# t print(primes)