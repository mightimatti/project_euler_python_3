
reps = {
	1 : "one",
	2 : "two",
	3 : "three",
	4 : "four",
	5 : "five",
	6 : "six",
	7 : "seven",
	8 : "eight",
	9 : "nine",
	10 : "ten",
	11 : "eleven",
	12 : "twelve",
	13 : "thirteen",
	14 : "fourteen",
	15 : "fifteen",
	16 : "sixteen",
	17 : "seventeen",
	18 : "eighteen",
	19 : "nineteen",
	20 : "twenty",
	30 : "thirty",
	40 : "forty",
	50 : "fifty",
	60 : "sixty",
	70 : "seventy",
	80 : "eighty",
	90 : "ninety",
	100 : "onehundred",
	200 : "twohundred",
	300 : "threehundred",
	400 : "fourhundred",
	500 : "fivehundred",
	600 : "sixhundred",
	700 : "sevenhundred",
	800 : "eighthundred",
	900 : "ninehundred",
	1000 : "onethousand"
}
 
def get_word_repr(number):
	if number in reps.keys():
		return reps[number]
	elif number > 100:
		return get_word_repr_hundreds(number)
	else: 
		return get_word_repr_tens(number)

def get_word_repr_tens(number):
	if number in reps.keys():
		return reps[number]
	else:
	 	return "{}{}".format(
	 		reps[(number / 10) * 10],  
	 		reps[number % 10]
	 	) 

def get_word_repr_hundreds(number):
	if number in reps.keys():
		return reps[number]
	else:
	 	return "{}and{}".format(
	 		reps[(number / 100) * 100],  
	 		get_word_repr_tens(number % 100)
	 	) 


def main():
	summ = 0
	for i in range(1, 1001):
		word = get_word_repr(i)
		summ += len(word) 
		print("{},\t{}\t'{}'".format(i,len(word),word,))
	print summ

if __name__ == "__main__":
	main()