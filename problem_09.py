"""
Problem 9

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

"""

def main():
	summ = 1000
	for c in range(summ):
		z = summ - c
		x = 0
		y = c
		while x < y:
			# print("x:{0}\t y:{1}\t z:{2}".format(x,y,z,))
			assert x + y + z == summ
			if ( 
				(x < y and y < z ) and 
				(x ** 2) + (y ** 2) == (z ** 2)):
				return x,y,z,
			else:
				x += 1
				y -= 1


if __name__ == '__main__':
	x,y,z = main()
	print("{0} + {1} + {2} = 1000".format(x,y,z,))
	print("{0} x {1} x {2} = {3}".format(x,y,z,x*y*z,))