"""
Maximum path sum II
   
Problem 67

By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
"""

PATH_TO_TXT_FILE = 'assets/p067_triangle.txt'

def convert_text_to_triangle(path):
	ret = []
	with open(path) as txt_file:
		for line in txt_file:
			ret.append(
				[int(x) for x in line.split(' ')]
			)
	return ret
				
TRIANGLE = convert_text_to_triangle(PATH_TO_TXT_FILE)

def find_maximum_for_level(length, prev_level_maxs):
	ret_list = []
	for i in range(length+1):
		if prev_level_maxs[i] > prev_level_maxs[i+1]:
			ret_list.append(
				TRIANGLE[length][i] + prev_level_maxs[i] 
			)
		else:
			ret_list.append(
				TRIANGLE[length][i] + prev_level_maxs[i+1] 
			)
	return ret_list

def main(levels):
	maximum_path_to_root = {}
	level_list = [0]*( len(TRIANGLE[-1]) + 1)
	for i in range(levels):
		level_index = len(TRIANGLE) - (i+1)
		level_list = find_maximum_for_level(
			level_index,
			level_list
		)
	return max(level_list)

if __name__ == '__main__':
	print(main(len(TRIANGLE)))