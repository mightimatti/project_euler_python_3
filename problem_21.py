
def d(n):
	divs = get_divisors(n)
	# print(n, divs)

	return reduce(
		lambda x,y: x + y,
		divs,
		0
	)


def get_divisors(n):
	divis = [1]
	for i in range(2,n):
		if i in divis:
			break
		if n % i == 0: 
			if i != n/i:
				divis += ([i,n/i])
			else: 
				divis.append(i)
	return sorted(divis)

def main(n):
	summ = []
	res = {}
	for i in range(n):
		d_i = d(i)
		res[i] = d_i
		if (d_i < i and
				  res[d_i] == i):
			summ.append((d_i, i,))
	return summ


if __name__ == '__main__':
	res = main(10000)
	from termcolor import cprint
	for tup in res:
		cprint(tup, "green")
		cprint("\t{}".format(get_divisors(tup[0])), "red")
		cprint("\t{}".format(get_divisors(tup[1])), "red")

		cprint("\t{}".format(d(tup[0])), "blue")
		cprint("\t{}".format(d(tup[1])), "blue")
	cprint("*"*17, 'red')
	cprint(
		"*\t{}\t*".format(
			reduce(
				lambda x, tup: x + sum(tup),
				(res),
				0
			) 
		),
		"red"
	)
	cprint("*"*17, 'red')